FROM node:16 as node
WORKDIR /usr/src/app
COPY . .
RUN npm install -g @angular/cli
RUN npm install
ENV SRV_URL: "https://exam-spring-boot-serve-cyril.herokuapp.com/api/tutorials"
RUN ng build

FROM nginx:latest
COPY --from=node /usr/src/app/dist/mds-dds-exam-angular /usr/share/nginx/html